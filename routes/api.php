<?php

use App\Http\Controllers\Api\SignController;
use App\Http\Controllers\Api\QuotationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* LETREROS */
Route::post('signs/store',[SignController::class,'store'])->name('api.signs.store');
Route::post('signs/{sign}/show',[SignController::class,'show'])->name('api.signs.show');
Route::put('signs/{sign}',[SignController::class,'update'])->name('api.signs.update');
/* COTIZACIONES */
Route::post('quotations/{quotation}/send',[QuotationController::class,'send'])->name('quotations.send');
Route::post('quotations/client',[QuotationController::class,'client'])->name('api.quotation.client');
Route::post('quotations/add/{sign}',[QuotationController::class,'addItem'])->name('api.quotation.add.item');
Route::delete('quotations/remove/{quotation}/{sign}',[QuotationController::class,'removeItem'])->name('api.quotation.remove.item');