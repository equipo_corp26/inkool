<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\SignController;
use App\Models\Sign;
use Illuminate\Support\Facades\Route;

/* HOME */
Route::get('/',[HomeController::class,'index'])->name('home.index');
/* ABOUT US */
Route::get('/about-us',function(){
    return view('static.about');
})->name('about');
/* CONTACT US */
Route::get('contact',[ContactController::class,'index'])->name('contact.index');
Route::post('contact',[ContactController::class,'store'])->name('contact.store');
/* SIGNS */
Route::get('signs',[SignController::class,'index'])->name('signs.index');
Route::get('signs/create',[SignController::class,'create'])->name('signs.create');
Route::get('signs/{sign}/edit',[SignController::class,'edit'])->name('signs.edit');
/* CATEGORIES */
Route::get('categories/{category}',[CategoryController::class,'show'])->name('category.show');
/* QUOTATIONS */
Route::get('quotations',[QuotationController::class,'index'])->name('quotations.index');
