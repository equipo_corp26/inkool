@extends('layouts.app')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show rounded-0" role="alert" style="z-index: 9999;position: fixed;width: 100vw">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/contacto.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>CONTACTANOS AHORA</h2>
                        <div class="breadcrumb__links">
                            <a href="{{ route('home.index') }}">Inicio</a>
                            <span>Contacto</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact__form">
                        <form method="POST" action="{{ route('contact.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="text" name="name" required placeholder="Nombre">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="text" name="email" required placeholder="Correo">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="text" name="phone" placeholder="Telefono">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="text" name="company" placeholder="Empresa">
                                </div>
                                <div class="col-lg-12">
                                    <textarea required name="message" placeholder="Mensaje"></textarea>
                                    <button type="submit" class="site-btn">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 contactcol2">
                    <div class="section-title">
                        <h2 class="titlecontact">¿Tienes alguna pregunta?</h2>
                        <h3 class="titlecontact">contactanos y nuestro equipo especialista te ayudara en tu proyecto</h3>
                    </div>
                    <div class="contact__widget__item">
                        <div class="contact__widget__item__icon">
                            <img src="img/contact/ci-1.png" alt="">
                        </div>
                        <div class="contact__widget__item__text">
                            <h5>Llámanos y te brindaremos la mejor atención.</h5>
                            <a href="tel:+56963712120" target="_blank"><span>+56 9 63712120</span></a>
                        </div>
                    </div>
                    <div class="contact__widget__item">
                        <div class="contact__widget__item__icon">
                            <img src="img/contact/ci-2.png" alt="">
                        </div>
                        <div class="contact__widget__item__text">
                            <h5>Escríbenos un correo y te responderemos a la brevedad.</h5>
                            <a href="mailto:contacto@inkool.cl"
                                target="_blank"><span>ventas@inkool.cl</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->
    <div class="map">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.363727067457!2d-70.38525408502096!3d-23.627141684650233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x96afd4d605e1e277%3A0x52d670b9574124dd!2sPuerto%20Natales%205131%2C%20Antofagasta%2C%20Chile!5e0!3m2!1ses!2sve!4v1620069822331!5m2!1ses!2sve"
            height="460" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div> 
@stop