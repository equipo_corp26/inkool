<!DOCTYPE html>
<html>
<head>
    <title>Inkcool</title>
    <style>
        * {
        box-sizing: border-box;
        }
        body {
        margin: 0;
        }
        .cell{
        min-height:75px;
        flex-grow:1;
        flex-basis:100%;
        }
        .row{
        display:flex;
        justify-content:flex-start;
        align-items:stretch;
        flex-wrap:nowrap;
        padding:10px;
        }
        *{
        box-sizing:border-box;
        }
        body{
        margin:0;
        }
        #i0kg{
        background-image:linear-gradient(to right, rgb(255, 255, 255) 3.90625%, rgb(250, 250, 250) 99.2188%);
        background-repeat:repeat;
        background-position:left top;
        background-attachment:scroll;
        background-size:auto;
        text-align:center;
        flex:0 0;
        max-width:800px;
        margin:0 auto 0 auto;
        border:5px solid #f30000;
        border-radius:5px 5px 5px 5px;
        }
        #ibyx7{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        color:#ffffff;
        background-image:linear-gradient(#ff0000,#ff0000);
        background-repeat:repeat;
        background-position:left top;
        background-attachment:scroll;
        background-size:auto;
        }
        #it50g{
        color:black;
        width:100%;
        }
        #iwgt{
        align-self:center;
        }
        #it06q{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #ii042{
        flex-direction:column;
        justify-content:space-around;
        align-self:center;
        }
        #iv71h{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #imbm3{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        font-size:10px;
        }
        #ixnmf{
        padding:10px;
        text-align:justify;
        font-family:Arial Black, Gadget, sans-serif;
        }
        @media (max-width: 768px){
        .row{
            flex-wrap:wrap;
        }
        }
    </style>
</head>
<body>
    <div id="i0kg" class="row">
        <div class="cell">
          <div class="row" id="iewi">
            <div id="ias8u" class="cell">
              <div id="ibyx7">SOLICITUD DE COTIZACION #{{$data['slug']}}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="cell" id="inltq">
              <div id="ixnmf">
                Cotizacion solicitada el: {{$data['date']}}
              </div>
              <div id="ixnmf">
                Cliente: {{$data['client']['email']}}
              </div>
            </div>
          </div>
          @foreach ($items as $item)
            <div class="row" id="i4zm">
                <div class="cell" id="iclp">
                <img id="it50g" src="{{Storage::url($item['picture'])}}"/>
                </div>
                <div class="cell" id="iwgt">
                <div id="it06q">DETALLES</div>
                <div id="imbm3">
                    <ul style="text-align: left">
                        <li>Alto: {{$item['height']}}</li>
                        <li>Ancho: {{$item['width']}}</li>
                        <li>Material: {{$item['material']}}</li>
                        <li>Norma: {{$item['norm']}}</li>
                        <li>Tipo: {{$item['type']}}</li>
                    </ul>
                </div>
                </div>
                <div class="cell" id="ii042">
                <div class="row">
                    <div class="cell" id="i9k5t">
                    <div id="iv71h">CANTIDAD: {{$item['pivot']['qty']}}
                    </div>
                    </div>
                </div>
                </div>
            </div>
          @endforeach
        </div>
      </div>
</body>
</html>