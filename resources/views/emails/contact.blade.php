<!DOCTYPE html>
<html>
<head>
    <title>Inkcool</title>
    <style>
        * {
        box-sizing: border-box;
        }
        body {
        margin: 0;
        }
        .cell{
        min-height:75px;
        flex-grow:1;
        flex-basis:100%;
        }
        .row{
        display:flex;
        justify-content:flex-start;
        align-items:stretch;
        flex-wrap:nowrap;
        padding:10px;
        }
        #igp9{
        flex-basis:30%;
        }
        #i0sb{
        flex-basis:70%;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #ibmh{
        background-image:-webkit-linear-gradient(90deg, rgb(255, 255, 255) 3.90625%, rgb(250, 250, 250) 99.2188%);
        }
        #i6d2{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #iw1z2{
        padding:10px;
        }
        #i0kg{
        background-image:linear-gradient(to right, rgb(255, 255, 255) 3.90625%, rgb(250, 250, 250) 99.2188%);
        background-repeat:repeat;
        background-position:left top;
        background-attachment:scroll;
        background-size:auto;
        text-align:center;
        flex:0 0;
        max-width:800px;
        margin:0 auto 0 auto;
        border:5px solid #f30000;
        border-radius:5px 5px 5px 5px;
        }
        #iq0m2{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #iiqts{
        flex-basis:30%;
        }
        #i81o5{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #imrgh{
        flex-basis:70%;
        }
        #iumed{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #ivyzp{
        flex-basis:30%;
        }
        #iwzwn{
        padding:10px;
        }
        #i8iqh{
        flex-basis:70%;
        font-family:Arial Black, Gadget, sans-serif;
        }
        #i1ew6{
        background-image:-webkit-linear-gradient(90deg, rgb(255, 255, 255) 3.90625%, rgb(250, 250, 250) 99.2188%);
        }
        #ibyx7{
        padding:10px;
        font-family:Arial Black, Gadget, sans-serif;
        color:#ffffff;
        background-image:linear-gradient(#ff0000,#ff0000);
        background-repeat:repeat;
        background-position:left top;
        background-attachment:scroll;
        background-size:auto;
        }
        #iegtf{
        background-image:linear-gradient(#ff0000,#ff0000);
        background-repeat:repeat;
        background-position:left top;
        background-attachment:scroll;
        background-size:auto;
        justify-content:space-around;
        padding:5px 0 0 0;
        }
        #ieb6g{
        color:black;
        height:67px;
        }
        @media (max-width: 768px){
        .row{
            flex-wrap:wrap;
        }
        }

    </style>
</head>
<body>
    <div class="row" id="i0kg">
        <div class="cell">
          <div class="row">
            <div class="cell" id="ias8u">
              <div id="ibyx7">UN CLIENTE QUIERE COMUNICARSE CONTIGO
              </div>
            </div>
          </div>
          <div class="row" id="ibmh">
            <div class="cell" id="igp9">
              <div id="i6d2">CLIENTE 
              </div>
            </div>
            <div class="cell" id="i0sb">
              <div id="iw1z2">{{ $data['name'] }}
              </div>
            </div>
          </div>
          <div class="row" id="i1ew6">
            <div class="cell" id="ivyzp">
              <div id="iumed">CORREO
              </div>
            </div>
            <div class="cell" id="i8iqh">
              <div id="iwzwn">{{ $data['email'] }}
              </div>
            </div>
          </div>
          <div class="row" id="i1ew6">
            <div class="cell" id="ivyzp">
              <div id="iumed">TELEFONO
              </div>
            </div>
            <div class="cell" id="i8iqh">
              <div id="iwzwn">{{ $data['phone'] }}
              </div>
            </div>
          </div>
          <div class="row" id="i1ew6">
            <div class="cell" id="ivyzp">
              <div id="iumed">EMPRESA
              </div>
            </div>
            <div class="cell" id="i8iqh">
              <div id="iwzwn">{{ $data['company'] }}
              </div>
            </div>
          </div>
          <div class="row" id="i9ye7">
            <div class="cell" id="iiqts">
              <div id="iq0m2">MENSAJE
              </div>
            </div>
            <div class="cell" id="imrgh">
              <div id="i81o5">{{ $data['message'] }}
              </div>
            </div>
          </div>
        </div>
      </div>
</body>
</html>