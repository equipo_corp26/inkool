@props(['sign'])
<div style="height: 260px;background-repeat: no-repeat;background-size: contain;background-position: center;background-image: url({{Storage::url($sign->picture)}})">
</div>

<div style="position: absolute;top: 0;">
    <add-item-component slug="{{$sign->slug}}"></add-item-component>
</div>

<div class="row product-info">
    <div class="col-lg-12"><h5 class="text-truncate">{{$sign->title}}</h5></div>
    <div class="col-lg-9"><a href="{{ route('category.show', $sign->category) }}"><span>{{$sign->category->name}}</span></a></div>
    <div class="col-lg-3"><a class="addtocart" href="{{ route('signs.edit', $sign) }}">EDITAR</a></div>
</div>