@extends('layouts.app')

@section('content')
<div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/letrero-abierto.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Mi cotizacion</h2>
                    <div class="breadcrumb__links">
                        <a href="{{ route('home.index') }}">Inicio</a>
                        <span>Mi cotizacion</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="about spad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <quotations-client-component/>
            </div>
        </div>
    </div>
</section>
@stop