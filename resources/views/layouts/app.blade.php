<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Aldrich&display=swap" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Inkool') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sweetalert.js/') }}" defer></script>
    <script src="{{ asset('js/jquery.textfill.min.js/') }}" defer></script>
    <!-- Js Plugins -->
    <script src="{{asset('js/jquery.slicknav.js')}}" defer></script>
    <script src="{{asset('js/owl.carousel.min.js')}}" defer></script>
    <script src="{{asset('js/slick.min.js')}}" defer></script>
    <script src="{{asset('js/html2canvas.min.js')}}" defer></script>
    <script src="{{asset('js/main.js')}}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body class="homepage">
    @include('partials.preloader')
    @include('partials.menu')
    
    
    <main id="app">
        @yield('content')
    </main>
    
    @include('partials.footer')
</body>
</html>
