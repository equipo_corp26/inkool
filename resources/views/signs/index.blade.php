@extends('layouts.app')

@section('content')
<div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/letrero-abierto.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>CATÁLOGO</h2>
                    <div class="breadcrumb__links">
                        <a href="{{ route('home.index') }}">Inicio</a>
                        <span>Catálogo</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="contact spad shop">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="titleshop">NUESTRO CATÁLOGO
                    </h2>
                    <span>MOSTRANDO {{$signs->count()}} DE {{$signs->total() }} PRODUCTOS</span>
                </div>
            </div>
        </div>
        <div class="row filterbar">
            <div class="col-lg-2 category-selector">
                <h5 class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CATEGORÍAS <i class="fa fa-list"></i></h5>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach ($categories as $category)
                        <a href="{{ route('category.show', [$category]) }}" class="dropdown-item text-capitalize font-weight-bold" href="#">{{$category->name}}</a>
                        @foreach ($category->categories as $children)
                            <a href="{{ route('category.show', [$children]) }}" class="dropdown-item text-capitalize" href="#"> - {{$children->name}}</a>
                        @endforeach

                        @if (!$loop->last)
                            <div class="dropdown-divider"></div>
                        @endif
                    @endforeach    
                </div>
            </div>
        </div>
        <div class="productgrid">
            <div class="row">
                @foreach ($signs as $sign)
                    <div class="col-lg-3 producto mb-5">
                        <x-card-sign :sign="$sign"></x-card-sign>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="row paginacion">
            <div class="col-lg-12">
                @if ($signs->previousPageUrl())
                    <a><i class="fa fa-chevron-left"></i></a>
                    <a href="{{ $signs->previousPageUrl() }}">PAGINA ANTERIOR</a>
                @endif
                <a>{{$signs->currentPage()}}</a>
                @if ($signs->nextPageUrl())
                    <a href="{{ $signs->nextPageUrl() }}">PAGINA SIGUIENTE</a>
                    <a><i class="fa fa-chevron-right"></i></a>
                @endif
            </div>
        </div>
    </div>
</section>
@stop
