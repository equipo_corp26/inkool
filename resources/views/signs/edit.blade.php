@extends('layouts.app')

@section('content')
<div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/letrero-abierto.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>DISEÑA TU LETRERO</h2>
                    <div class="breadcrumb__links">
                        <a href="{{ route('home.index') }}">Inicio</a>
                        <span>DISEÑA TU LETRERO</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="about spad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <signs-customization-component
                    sign_prop={{$sign}}
                    :types="{{$types}}"
                    :norms="{{$norms}}"
                    :colors="{{$colors}}"
                    :pictograms="{{$pictograms}}"
                    :materials="{{$materials}}"
                />
            </div>
        </div>
    </div>
</section>
    
@stop