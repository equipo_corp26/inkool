@extends('layouts.app')

@section('content')
    <div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/letrero-abierto.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>{{$category->name}}</h2>
                        <div class="breadcrumb__links">
                            <a href="{{ route('home.index') }}">Inicio</a>
                            <span>{{$category->name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="contact spad shop">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2 class="titleshop text-uppercase">NUESTRO CATÁLOGO DE {{$category->name}}</h2>
                            </h2>
                            <span>MOSTRANDO {{$signs->count()}} DE {{$signs->total() }} PRODUCTOS</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="productgrid">
                <div class="row justify-content-center">
                    @foreach ($signs as $sign)
                        <div class="col-lg-3 mb-3 producto">
                            <x-card-sign :sign="$sign"></x-card-sign>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row paginacion">
                <div class="col-lg-12">
                    @if ($signs->previousPageUrl())
                        <a><i class="fa fa-chevron-left"></i></a>
                        <a href="{{ $signs->previousPageUrl() }}">PAGINA ANTERIOR</a>
                    @endif
                    <a>{{$signs->currentPage()}}</a>
                    @if ($signs->nextPageUrl())
                        <a href="{{ $signs->nextPageUrl() }}">PAGINA SIGUIENTE</a>
                        <a><i class="fa fa-chevron-right"></i></a>
                    @endif
                </div>
            </div>
        </div>
    </section>
@stop
