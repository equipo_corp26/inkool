@extends('layouts.app')

@section('content')
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option spad set-bg overlay-bg" data-setbg="{{asset('img/recursos/letrero-abierto.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>SOMOS GRAFICA INKCOOL</h2>
                        <h4 class="text-white">Fabrica Señaletica Preventiva, Caminera e Indusrial</h4>
                        <div class="breadcrumb__links">
                            <a href="{{ route('home.index') }}">Inicio</a>
                            <span>Nosotros</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- About Section Begin -->
    <section class="about spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about__para__text2">
                        <img src="{{asset('img/recursos/senal-construccion.jpg')}}" alt="" class="about-img">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about__text">
                        <div class="about__para__text">
                            <img src="{{asset('img/recursos/aviso-camion.jpg')}}" alt="" class="about-img">
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <h2>Señalizando la Industria, los caminos y la Minería desde el año 2007</h2>
                </div>
                <div class="col-lg-6">
                    <h4 class="mision-vision-title">POLITICA SSO</h4>
                    <p class="text-justify">Nuestra política esta orientada al aseguramiento de la calidad, a la protección Ambiental y a la Seguridad y Salud Ocupacional, siendo esta la base del compromiso con la mejora continua de nuestra organización. 
                        Inkcool, dentro de la mejora continua estipulada en su sistema de gestión integrado se compromete a cumplir con lo siguiente: </p>
                        <ul style="font-size: 15px;font-family: 'Poppins', sans-serif;color: #707070;font-weight: 400;line-height: 26px;margin: 0 0 15px 0;">
                            <li>Satisface las necesidades de las organizaciones clientes.</li>
                            <li>Desarrollar las actividades dentro de nuestras instalaciones, como en las de nuestros clientes, controlando siempre los riesgos, aspectos ambientales y a no interferir en los procesos de aseguramiento de la calidad de ambas partes  </li>
                            <li>Prevenir y controlar los incidentes, enfermedades ocupacionales, contaminación y otros factores que afecten en la calidad de nuestros trabajadores y de nuestros servicios. </li>
                        </ul>
                </div>
                <div class="col-lg-6">
                    <div class="about__text">
                        <div class="about__para__text">
                            <img src="{{asset('img/recursos/somos_3.png')}}" alt="" class="about-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->
@stop