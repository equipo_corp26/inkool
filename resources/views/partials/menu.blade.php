<!-- Offcanvas Menu Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="offcanvas__logo">
        <a href="{{ route('home.index') }}">
            <img src="{{asset('img/recursos/logo.png')}}" alt="" width="165" height="auto">
        </a>
    </div>
    <div id="mobile-menu-wrap"></div>
    <div class="offcanvas__widget">
        <a href="{{ route('quotations.index') }}"><img src="{{asset('img/iconos/shopping-cart.svg')}}" width="32px" height="auto" alt="carrito"></a>
    </div>
</div>
<!-- Offcanvas Menu End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="{{ route('home.index') }}">
                        <img src="{{asset('img/recursos/logo.png')}}" alt="" width="165" height="auto">
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu mobile-menu">
                    <ul>
                        <li class="{{ request()->is('/')            ? 'active' : '' }}" ><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="{{ request()->is('about-us')    ? 'active' : '' }}" ><a href="{{ route('about') }}">Nosotros</a></li>
                        <li class="{{ request()->is('signs')       ? 'active' : '' }}" ><a href="{{ route('signs.index')}}">Tienda</a></li>
                        <li class="{{ request()->is('contact')     ? 'active' : '' }}" ><a href="{{ route('contact.index') }}">Contacto</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__widget mt-1">
                    <a href="{{ route('signs.create') }}" class="text-white border border-white p-2 font-weigth-bold" style="font-weight: bold;font-size: 16px">DISEÑA TU LETRERO</a>
                    <a href="{{ route('quotations.index') }}"><img src="{{asset('img/iconos/shopping-cart.svg')}}" width="32px" height="auto" alt="carrito"></a>
                </div>
            </div>
        </div>
        <div class="canvas__open"><i class="fa fa-bars"></i></div>
    </div>
</header>
<!-- Header Section End -->