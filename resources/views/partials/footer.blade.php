<!-- Footer Section Begin -->
<footer class="footer set-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="footer__about">
                    <div class="footer__logo">
                        <a href="/"><img src="{{asset('img/recursos/logo.png')}}" alt="" width="165" height="auto"></a>
                    </div>
                    <p class="text-justify">Somos especialistas en la fabricación
                        de sistemas de Señalización Industrial
                        para la prevención continua de
                        Accidentes laborales en industria y minería.</p>
                    <div class="footer__social">
                        <a href="#"><i class="fa-2x fa fa-facebook"></i></a>
                        <a href="#"><i class="fa-2x fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="footer__widget">
                    <h6>Contáctanos</h6>
                    <ul>
                        <li><a target="_blank" href="tel:+56963712120"><i class="fa fa-phone-square"></i> +56 9 63712120</li>
                        <li><a target="_blank" href="mailto:contacto@inkcool.cl"><i class="fa fa-envelope"></i> ventas@inkcool.cl</a></li>
                        <li><a href="https://goo.gl/maps/rjtEwWjS3G4tbzTs9"><i class="fa fa-map" aria-hidden="true"></i> Calle Puerto Natales #5131
                            Población Lautaro
                            Antofagasta
                            </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="footer__widget">
                    <h6>Enlaces</h6>
                    <ul>
                        <li><a href="{{ route('home.index')}}">Home</a></li>
                        <li><a href="{{ route('about')}}">Nosotros</a></li>
                        <li><a href="{{ route('contact.index')}}">Contacto</a></li>
                        <li><a href="{{ route('signs.index')}}">Productos</a></li>
                        <li><a href="#">Políticas</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 text-center">
                <div class="footer__widget footer-widget-last">
                    <a href="https://wa.me/+56963712120" target="_blank">
                        <img src="{{ asset('img/recursos/ws.png') }}" alt="logo whatsapp" width="100px">
                        <h4 class="text-white font-weight-bold">¡Contactanos Ahora!</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <div class="copyright__text">
                    <a href="#">Términos y condiciones</a>
                    <a href="#">Política de Privacidad</a>
                </div>
                
            </div>
            <div class="col-lg-4 col-md-5">
                <div class="copyright__widget">
                    <p>INKCOOL© Copyright
                        {{date('Y')}} by <a href="https://www.agenciavimos.com/"
                            target="_blank">Agencia Vimos</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->