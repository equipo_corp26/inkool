@extends('layouts.app')

@section('content')
    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="hero__slider owl-carousel">
            <div class="hero__items set-bg" data-setbg="{{asset('img/recursos/slider-1.png')}}">
                <div class="hero__text">
                    <h2>Especialistas <br>en la  Industria</h2>
                    <a href="{{ route('signs.index')}}" class="primary-btn">Ir a comprar</a>
                    <div class="hero__social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('img/recursos/slider-2.png')}}">
                <div class="hero__text">
                    <h2>Señaletica <br>Industrial y Minera</h2>
                    <a href="{{ route('signs.index')}}" class="primary-btn">Ir a comprar</a>
                    <div class="hero__social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('img/recursos/slider-3.png')}}">
                <div class="hero__text">
                    <h2>Señales de Alta <br> Reflectancia</h2>
                    <a href="{{ route('signs.index')}}" class="primary-btn">Ir a comprar</a>
                    <div class="hero__social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- About Section Begin -->
    <section class="about spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <a style="position: absolute;top: 50%;left: 35%;" href="{{ route('signs.create') }}" class="my-auto btn btn-lg btn-danger rounded-0">Diseña tu letrero</a>
                </div>
                <div class="col-lg-6">
                    <div class="about__text">
                        <h2>Gráfica INKCOOL desde el 2005 brindando lo mejor.</h2>  
                        <div class="about__para__text">
                            <p>Gráfica INKCOOL, nace en Abril de 2005 con el impulso claro de fabricar señalización
                                preventiva principalmente para industrias y minería, luego expandimos este servicio a
                                empresas medianas contratistas que prestan servicios no solo a minería sino a toda la
                                industria en general, todos ellos se interesaron en la calidad del producto el cual
                                ofrece una duración prolongada en el tiempo en condiciones climéticas muchas veces
                                adversas.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 senales">
                    <img src="{{asset('img/recursos/senales.png')}}" alt="Señales">
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- About Banner Section Begin -->
    <section class="about about2 spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 my-3">
                    <img src="{{asset('img/recursos/capa-1.png')}}" alt="">
                </div>
                <div class="col-lg-4 my-3">
                    <img src="{{asset('img/recursos/capa-2.png')}}" alt="">
                </div>
                <div class="col-lg-4 my-3">
                    <img src="{{asset('img/recursos/capa-3.png')}}" alt="">
                </div>
                <div class="col-lg-4 my-3">
                    <a style="position: absolute;top: 50%;left: 35%;" href="{{ route('signs.create') }}" class="my-auto btn btn-lg btn-danger rounded-0">Diseña tu letrero</a>
                </div>
                <div class="col-lg-8 my-3">
                    <img src="{{asset('img/recursos/inkool.png')}}" alt="">
                </div>
                <div class="col-lg-6 my-3">
                    <div class="row">
                        <div class="col-lg-2 text-center">
                            <img src="{{ asset('img/recursos/ico-1.png') }}" alt="">
                        </div>
                        <div class="col-lg-10">
                            <h4>FABRICA DE SEÑALÉTICA PREVENTIVA</h4>
                            <p class="d-inline-block"> (Galvanizado, Plástico, Madera y Acrílica)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="row">
                        <div class="col-lg-2 text-center">
                            <img src="{{ asset('img/recursos/ico-2.png') }}" alt="">
                        </div>
                        <div class="col-lg-10">
                            <h4 class="d-block">GRABADO y CORTE LASER</h4>
                            <p class="d-block">(Galvano, Buzones de seguridad, pizarras, etc)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="row">
                        <div class="col-lg-2 text-center">
                            <img src="{{ asset('img/recursos/ico-3.png') }}" alt="">
                        </div>
                        <div class="col-lg-10">
                            <h4 class="d-block">GRABADO y CORTE LASER</h4>
                            <p class="d-block">(Galvano, Buzones de seguridad, pizarras, etc)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="row">
                        <div class="col-lg-2 text-center">
                            <img src="{{ asset('img/recursos/ico-4.png') }}" alt="">
                        </div>
                        <div class="col-lg-10">
                            <h4 class="d-block">ROUTER CNC</h4>
                            <p class="d-block">(Corte automatizado de maderas, acrílicos y plásticos)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- Project Section Begin -->
    <section class="project">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">
                        <h2>Productos Destacados</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="project__slider owl-carousel">
                    @foreach ($signs as $sign)
                    <div class="col-lg-3">
                        <div class="project__slider__item set-bg" data-setbg="{{Storage::url($sign->picture)}}">
                            <div class="project__slider__item__hover text-capitalize">
                                <a class="product-link" href="{{ route('category.show', $sign->category) }}"><small>{{$sign->category->name}}</small></a>
                                <span><small>{{$sign->title}}</small></span>
                                <add-item-component slug="{{$sign->slug}}"></add-item-component>
                                <a href="{{ route('signs.edit', [$sign]) }}" class="btn btn-outline-primary" href="producto.html"><i class="fa fa-edit"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-lg-12 btn-tienda">
                    <div class="callto__text">
                        <a href="{{ route('signs.index')}}" class="primary-btn about2btn">Ir a la tienda</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Project Section End -->

    <!-- About Banner Section Begin -->
    <section class="about about3 spad">
        <div class="row">
            <div class="col-lg-5 colwithbg">
                <span class="spacer"></span>
            </div>
            <div class="col-lg-7 colwithoutbg">
                <div class="about__para__text4">
                    <h3>Somos fabricantes de sistemas de señalización preventiva industrial </h3>
                    <div class="about__text">
                        <div class="about__para__text5">
                            <p>Nuestros materiales son probados constantemente en diferentes condiciones climaticas, esto garantiza su duración en el tiempo en condiciones de faena adversas.</p>
                        </div>
                        <div class="col-lg-12">
                            <div class="callto__text">
                                <a href="{{ route('signs.index')}}" class="primary-btn about3btn">Ir a la tienda</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->
@stop