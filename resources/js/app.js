/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
/* SINGS */
Vue.component('signs-customization-component', require('./components/signs/customizationComponent.vue').default);
/* QUOTATIONS */
Vue.component('quotations-client-component', require('./components/quotations/clientComponent.vue').default);
Vue.component('add-item-component', require('./components/quotations/addItemComponent.vue').default);

const app = new Vue({
    el: '#app'
});
