<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuotationRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $items;

    public function __construct($data,$items)
    {
        $this->data     = $data;
        $this->items    = $items;
    }

    public function build()
    {
        return $this->from($this->data['client']['email'])
                    ->subject('Solicitud Cotizacion')
                    ->view('emails.quotations');
    }
}
