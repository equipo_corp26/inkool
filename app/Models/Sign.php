<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Sign extends Model
{ 
    use HasFactory,
        Sluggable;
    /* CAMPOS */
    protected $fillable = [
        'code',
        'slug',
        'norm',
        'size',
        'color',
        'color_line',
        'color_text',
        'pictogram',
        'picture',
        'header_type',
        'header_color',
        'header_color_text',
        'header_size',
        'title',
        'sub_title',
        'text1',
        'text2',
        'text3',
        'text4',
        'comment',
        'material',
        'width',
        'height',
        'type',
        'status',
        'client_id',
        'category_id',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'code']];
    }
    /* RELACIONES */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function quotations()
    {
        return $this->belongsToMany(Quotation::class)->withPivot('qty');
    }
}
