<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    /* CAMPOS */
    protected $fillable = [
        'email'
    ];
    /* RELACIONES */
    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }
    public function signs()
    {
        return $this->hasMany(Sign::class);
    }
}
