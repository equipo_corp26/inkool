<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use HasFactory,
        Sluggable;
    /* CAMPOS */
    protected $fillable = [
        'name',
        'slug',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
    /* RELACIONES */
    public function signs()
    {
        return $this->hasMany(Sign::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'id');
    }
    public function categories()
    {
        return $this->hasMany(Category::class,'parent');
    }
}
