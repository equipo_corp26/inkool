<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Material extends Model
{
    use HasFactory,
        Sluggable;
    /* CAMPOS */
    protected $fillable = [
        'name',
        'slug',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
    /* RELACIONES */
   public function dimensions()
    {
        return $this->belongsToMany(Dimension::class);
    }
}
