<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Dimension extends Model
{
    use HasFactory,
        Sluggable;
    /* CAMPOS */
    protected $fillable = [
        'slug',
        'width',
        'height',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return [ 'slug' => ['source' => ['width','height','id'] ] ];
    }
    /* RELACIONES */
    public function materials()
    {
        return $this->belongsToMany(Material::class);
    }
}
