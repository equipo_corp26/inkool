<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Header extends Model
{
    use HasFactory,
        Sluggable;    
    /* CAMPOS */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'size',
        'color',
        'title',
        'custom',
        'sub_title',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
    /* RELACIONES */
    public function norms()
    {
        return $this->belongsToMany(Norm::class);
    }
}
