<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Quotation extends Model
{
    use HasFactory,
        Sluggable;
     /* CAMPOS */
     protected $fillable = [
        'date',
        'code',
        'slug',
        'status',
        'client_id',
    ];
    /* KEYNAME */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /* SLUG */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'code']];
    }
    /* RELACIONES */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function signs()
    {
        return $this->belongsToMany(Sign::class)->withPivot('qty');
    }
}