<?php

namespace App\Http\Controllers;

use App\Http\Resources\ColorResource;
use App\Http\Resources\MaterialResource;
use App\Http\Resources\NormResource;
use App\Http\Resources\PictogramResource;
use App\Http\Resources\SignResource;
use App\Http\Resources\TypeResource;
use App\Models\Category;
use App\Models\Color;
use App\Models\Material;
use App\Models\Norm;
use App\Models\Pictogram;
use App\Models\Sign;
use App\Models\Type;
use Illuminate\Http\Request;

class SignController extends Controller
{
    public function index()
    {
        $signs = Sign::where('category_id','!=',9)->inRandomOrder()->paginate(12);
        $categories = Category::where('id','!=',9)->where('parent',0)->with('categories')->latest('name')->get(['slug','name','id','parent']);
        return view('signs.index',compact('signs','categories'));
    }
    public function create()
    {
        $colors         = Color::all(['name','code','slug','line_code'])->toJson();
        $pictograms     = Pictogram::all(['url'])->toJson();
        $norms          = Norm::with('headers')->latest('name')->get()->toJson();
        $types          = Type::latest('name')->get(['name','slug'])->toJson();
        $materials      = Material::with('dimensions')->latest('name')->get(['id','name','slug'])->toJson();
        /* RETURN */
        return view('signs.create',compact('colors','pictograms','norms','types','materials'));
    }
    public function edit(Sign $sign)
    {
        $sign           = $sign->slug;
        $colors         = Color::all(['name','code','slug','line_code'])->toJson();
        $pictograms     = Pictogram::all(['url'])->toJson();
        $norms          = Norm::with('headers')->latest('name')->get()->toJson();
        $types          = Type::latest('name')->get(['name','slug'])->toJson();
        $materials      = Material::with('dimensions')->latest('name')->get(['id','name','slug'])->toJson();
        /* RETURN */
        return view('signs.edit',compact('colors','pictograms','norms','types','materials','sign'));
    }
}
