<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\QuotationRequest;
use App\Models\Client;
use App\Models\Quotation;
use App\Models\Sign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class QuotationController extends Controller
{
    public function client(Request $request)
    {
        $quotation = 0;
        if($request->email)
        {
            $client = Client::where('email',$request->email)->first();
            if($client)
            {
                $quotation = Quotation::with('signs')
                                        ->where('client_id',$client->id)
                                        ->where('status','draft')
                                        ->first(['code','date','slug','id','status']);
            }
            return $quotation;
        }else{
            return 0;
        }
    }
    public function addItem(Sign $sign,Request $request)
    {
        /* VEO SI EL CLIENTE EXISTE O LO CREO */
        $client = Client::firstOrCreate([
            'email' => $request->email
        ]);
        /* AGREGO SIGN A COTIZACION */
        $quotation = Quotation::where('client_id',$client->id)->where('status','draft')->first();

        if($quotation == null)
        {
            $quotation = Quotation::create([
                'code'          => date('ymd').rand(10000,99999).date('hsi'),
                'client_id'     => $client->id,
                'status'        => 'draft',
                'date'          => now()
            ]);
        }
        /* ENLAZAR EL QUOTATION Y EL SIGN */
        $quotation->signs()->attach([
            $sign->id => ['qty' => $request->qty] 
        ]);
        /* RETURN */
        return $quotation->signs;
    }
    public function send(Quotation $quotation)
    {
        $quotation->load('client');
        $signs = $quotation->signs;
        $mail = new QuotationRequest($quotation,$signs);
        Mail::to('inkool@agenciavimos.com')->send($mail);
        /* Set signs  */
        $id_draft = $signs->where('status','draft')->pluck('id');
        Sign::whereIn('id', $id_draft)->update(['status' => 'published']);
        /* set quotation */
        $quotation->update(['status'=>'emitted']);
        /* return */
        return back()->with(['success' => 'Mensaje enviado con exito, recibira su respuesta lo antes posible.']);
    }
    public function removeItem(Quotation $quotation,Sign $sign)
    {
        if($sign->category->slug == 'personalizado')
        {
            Storage::delete($sign->picture);
            $sign->delete();
        }
        return $quotation->signs()->detach($sign->id);
    }
}
