<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignRequest;
use App\Http\Resources\SignResource;
use App\Models\Category;
use App\Models\Client;
use App\Models\Dimension;
use App\Models\Header;
use App\Models\Material;
use App\Models\Norm;
use App\Models\Pictogram;
use App\Models\Quotation;
use App\Models\Sign;
use App\Models\Type;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SignController extends Controller
{
    public function store(SignRequest $request)
    {
        /* VEO SI EL CLIENTE EXISTE O LO CREO */
        $client = Client::firstOrCreate([
            'email' => $request->email
            ]);
        /* PICTURE */
        $data = substr($request->picture, strpos($request->picture, ',') + 1);
        $data = base64_decode($data);
        $folder = 'demos/'.rand(1000,9999).date('ymdhsi').rand(1000,9999).'.jpg';
        Storage::disk('public')->put($folder,$data);
        /* CATEGORIA */
        $category = Category::where('slug','personalizado')->first()->id;
        $sign = Sign::create([
            'code'                  => date('ymd').rand(10000,99999).date('hsi'),
            'norm'                  => $request->norm,
            'size'                  => $request->size,
            'color'                 => $request->color,
            'color_line'            => $request->color_line,
            'color_text'            => $request->color_text,
            'pictogram'             => $request->pictogram,
            'header_type'           => $request->header_type,
            'header_color'          => $request->header_color,
            'header_color_text'     => $request->header_color_text,
            'header_size'           => $request->header_size,
            'title'                 => $request->title,
            'sub_title'             => $request->sub_title,
            'text1'                 => $request->text1,
            'text2'                 => $request->text2,
            'text3'                 => $request->text3,
            'text4'                 => $request->text4,
            'comment'               => $request->comment,
            'material'              => $request->material,
            'width'                 => $request->width,
            'height'                => $request->height,
            'type'                  => $request->type,
            'picture'               => $folder,
            'client_id'             => $client->id,
            'category_id'           => $category,
        ]);
        /* AGREGO SIGN A COTIZACION */
        $quotation = Quotation::where('client_id',$client->id)->where('status','draft')->first();

        if($quotation == null)
        {
            $quotation = Quotation::create([
                'code'          => date('ymd').rand(10000,99999).date('hsi'),
                'client_id'     => $client->id,
                'status'        => 'draft',
                'date'          => now()
            ]);
        }
        /* ENLAZAR EL QUOTATION Y EL SIGN */
        $quotation->signs()->attach([
            $sign->id => ['qty' => $request->qty] 
        ]);
        /* RETURN */
        return $sign->id;
    }
    public function update(Sign $sign,SignRequest $request)
    {
        /* VEO SI EL CLIENTE EXISTE O LO CREO */
        $client = Client::firstOrCreate([
            'email' => $request->email
        ]);
        /* PICTURE */
        $data = substr($request->picture, strpos($request->picture, ',') + 1);
        $data = base64_decode($data);
        $folder = 'demos/'.rand(1000,9999).date('ymdhsi').rand(1000,9999).'.jpg';
        Storage::disk('public')->put($folder,$data);
        /* CATEGORIA */
        Storage::delete($sign->picture);
        $category   = Category::where('slug','personalizado')->first()->id;
        if($sign->category_id == $category && $sign->status == 'draft')
        {
            $sign->update([
                'norm'                  => $request->norm,
                'size'                  => $request->size,
                'color'                 => $request->color,
                'color_line'            => $request->color_line,
                'color_text'            => $request->color_text,
                'pictogram'             => $request->pictogram,
                'header_type'           => $request->header_type,
                'header_color'          => $request->header_color,
                'header_color_text'     => $request->header_color_text,
                'header_size'           => $request->header_size,
                'title'                 => $request->title,
                'sub_title'             => $request->sub_title,
                'text1'                 => $request->text1,
                'text2'                 => $request->text2,
                'text3'                 => $request->text3,
                'text4'                 => $request->text4,
                'comment'               => $request->comment,
                'material'              => $request->material,
                'width'                 => $request->width,
                'height'                => $request->height,
                'type'                  => $request->type,
                'picture'               => $folder,
            ]);
        }else{
            $sign = Sign::create([
                'code'                  => date('ymd').rand(10000,99999).date('hsi'),
                'norm'                  => $request->norm,
                'size'                  => $request->size,
                'color'                 => $request->color,
                'color_text'            => $request->color_text,
                'pictogram'             => $request->pictogram,
                'header_color_text'     => $request->header_color_text,
                'header_type'           => $request->header_type,
                'header_color'          => $request->header_color,
                'header_size'           => $request->header_size,
                'title'                 => $request->title,
                'sub_title'             => $request->sub_title,
                'text1'                 => $request->text1,
                'text2'                 => $request->text2,
                'text3'                 => $request->text3,
                'text4'                 => $request->text4,
                'comment'               => $request->comment,
                'material'              => $request->material,
                'width'                 => $request->width,
                'height'                => $request->height,
                'type'                  => $request->type,
                'client_id'             => $client->id,
                'category_id'           => $category,
                'picture'               => $folder,
            ]);
        }
        
        /* AGREGO SIGN A COTIZACION */
        $quotation = Quotation::where('client_id',$client->id)->where('status','draft')->first();

        if($quotation == null)
        {
            $quotation = Quotation::create([
            'code'          => date('ymd').rand(10000,99999).date('hsi'),
            'client_id'     => $client->id,
            'status'        => 'draft',
            'date'          => now()
            ]);
        }
        /* ENLAZAR EL QUOTATION Y EL SIGN */
        $quotation->signs()->detach($sign->id);
        $quotation->signs()->attach([
            $sign->id => ['qty' => $request->qty] 
        ]);
        /* RETURN */
        return $sign->id;
    }
    public function show(Sign $sign)
    {
        return response()->json($sign);
    }
}
