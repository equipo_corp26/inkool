<?php

namespace App\Http\Controllers;

use App\Models\Sign;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $signs = Sign::with('category:name,id,slug')->whereHas('category', function (Builder $query) {
            $query->where('slug', '!=', 'personalizado');
        })->inRandomOrder()->limit(6)->get(['slug','picture','category_id','title']);
        return view('home.index',compact('signs'));
    }
}
