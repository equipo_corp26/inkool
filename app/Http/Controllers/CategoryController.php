<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Category $category)
    {
        $signs = $category->signs()->inRandomOrder()->paginate(10);
        return view('categories.show',compact('signs','category'));
    }
}
