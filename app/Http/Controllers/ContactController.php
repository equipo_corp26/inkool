<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }
    public function store(Request $request)
    {
        $mail = new ContactUsMail($request->all());
        Mail::to('inkool@agenciavimos.com')->send($mail);
        return back()->with(['success' => 'Mensaje enviado con exito, recibira su respuesta lo antes posible.']);
    }
}
