<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'norm'              => 'nullable|exists:norms,slug',
            'size'              => 'nullable|numeric',
            'color'             => 'required|exists:colors,code',
            'color_line'        => 'required|exists:colors,line_code',
            'color_text'        => 'required|exists:colors,code',
            'pictogram'         => 'nullable|exists:pictograms,url',
            'header_type'       => 'nullable',
            'header_color'      => 'nullable|exists:colors,code',
            'header_color_text' => 'nullable|exists:colors,code',
            'header_size'       => 'nullable|max:80',
            'title'             => 'nullable|max:80',
            'sub_title'         => 'nullable|max:80',
            'text1'             => 'nullable|max:80',
            'text2'             => 'nullable|max:80',
            'text3'             => 'nullable|max:80',
            'text4'             => 'nullable|max:80',
            'comment'           => 'nullable|max:80',
            'material'          => 'required|exists:materials,slug',
            'width'             => 'required',
            'height'            => 'required',
            'type'              => 'required|exists:types,slug',
            'email'             => 'required|email',
            'qty'               => 'required|numeric'
        ];
    }
}
