<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HeaderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name'      => $this->name,
            'slug'      => $this->slug,
            'type'      => $this->type,
            'size'      => $this->size,
            'color'     => $this->color,
            'title'     => $this->title,
            'custom'    => $this->custom,
            'sub_title' => $this->sub_title,
        ];
    }
}
