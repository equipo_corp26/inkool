<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PictogramResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'url'           => $this->url,
            'url_complete'  => Storage::url( $this->url )
        ];
    }
}
