<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SignResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'text'          => $this->text, 
            'code'          => $this->code, 
            'slug'          => $this->slug, 
            'color'         => $this->color,
            'border'        => $this->border, 
            'status'        => $this->status, 
            'type'          => $this->type->slug, 
            'norm'          => $this->norm_id == null ? '' :  $this->norm->slug , 
            'header'        => new HeaderResource($this->header), 
            'client'        => $this->client->email, 
            'category'      => $this->category->slug, 
            'material'      => $this->material->slug, 
            'pictogram'     => $this->pictogram_id == null ? '' :  $this->pictogram->url ,  
            'dimension'     => $this->dimension->slug,
            'comment'       => $this->comment,
        ];
    }
}
