<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NormResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name'              => $this->name,
            'slug'              => $this->slug,
            'headers'           => HeaderResource::collection($this->headers),
            'background_type'   => $this->background_type,
        ];
    }
}
