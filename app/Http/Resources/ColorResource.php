<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ColorResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name'  => $this->name,
            'slug'  => $this->slug,
            'code'  => $this->code,
            'line_code'  => $this->line_code,
        ];
    }
}
