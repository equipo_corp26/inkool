<?php

namespace App\Http\Resources;

use App\Models\Quotation;
use Illuminate\Http\Resources\Json\JsonResource;

class QuotationResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'client'    => $this->client->email,
            'code'      => $this->code,
            'slug'      => $this->slug,
            'status'    => $this->status,
            'date'      => $this->date,
            'items'     => SignResource::collection($this->signs)   ,
            'signs'     => $this->signs   
        ];
    }
}
