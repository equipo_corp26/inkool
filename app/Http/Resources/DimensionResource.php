<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DimensionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'slug'      => $this->slug,
            'width'     => $this->width,
            'height'    => $this->height,
        ];
    }
}
