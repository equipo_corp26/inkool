###### Fecha de liberación: **25 de Mayo del 2021**
# Notas de la version v2.1.0

Actualmente la pagina web tiene funcional las siguientes vistas:

## Home 
Se muestra información referente a la empresa con enlaces a las vista de tienda y lista 6 letrero de la tienda de forma aleatoria en un carrusel.

## Nosotros
Pagina informativa sin funciones adicionales.

## Tienda
Se listan los letreros paginados y con las opciones de agregar al carro o personalizarlos, también permite filtrar los letreros por categorías 

` **Importante** Los letreros que se muestran no son los finales, todavía no se cargan los letreros predefinidos reales`. 

## Contacto
Muestra la información de contacto , junto a un mapa con Google maps con la dirección y un formulario de contacto que permite enviar un correo a la empresa.

## Crear Letrero
Se puede crear un letrero desde cero con estilos personalizados según factores como, color , cabecera, títulos , subtitulo, pictogramas, material, tamaño, textos. 

` **Importante** Los pictogramas que se muestran no son los finales, todavía no se cargan los pictogramas reales`.

## Carro de Compras
Lista todos los letreros agregados al carro, desde los predefinidos por la pagina, hasta los creados desde cero y personalizados. Desde aquí se puede enviar por correo la cotización para de los productos en el carro a la empresa para posteriormente recibir la respuesta por correo.
