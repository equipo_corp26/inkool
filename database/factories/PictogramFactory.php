<?php

namespace Database\Factories;

use App\Models\Pictogram;
use Illuminate\Database\Eloquent\Factories\Factory;

class PictogramFactory extends Factory
{
    protected $model = Pictogram::class;

    public function definition()
    {
        return [
            'url' => 'pictograms-folder/' . $this->faker->unique()->image('public/storage/pictograms-folder',300,300,null,false)
        ];
    }
}
