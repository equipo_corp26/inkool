<?php

namespace Database\Factories;

use App\Models\Norm;
use App\Models\Pictogram;
use App\Models\Sign;
use Illuminate\Database\Eloquent\Factories\Factory;

class SignFactory extends Factory
{
    protected $model = Sign::class;

    public function definition()
    {
        return [
            'code'          => strtoupper($this->faker->lexify('??????????')),
            'norm'          => Norm::find(1)->slug,
            'size'          => $this->faker->randomElement([2,3,4]),
            'color'         => $this->faker->randomElement(['#000000','#ff0000','#00ff00','#0000ff','#ffffff','#f0f000','#ffa500']),
            'pictogram'     => Pictogram::find(1)->url,
            'header_type'   => $this->faker->randomElement(['normal','oval']),
            'header_color'  => $this->faker->randomElement(['#000000','#ff0000','#00ff00','#0000ff','#ffffff','#f0f000','#ffa500']),
            'header_size'   => 'big',
            'title'         => $this->faker->sentence(1),
            'sub_title'     => $this->faker->sentence(1),
            'text1'         => $this->faker->sentence(1),
            'text2'         => $this->faker->sentence(1),
            'text3'         => $this->faker->sentence(1),
            'text4'         => $this->faker->sentence(1),
            'comment'       => $this->faker->sentence(4),
            'material'      => 'autoadhesivo',
            'width'         => 42,
            'height'        => 30,
            'type'          => 'normal',
            'status'        => 'published',
            'category_id'   => $this->faker->numberBetween(1,9),
            'picture'       => 'demos/' . $this->faker->unique()->image('public/storage/demos',350,280,null,false)
        ];
    }
}
