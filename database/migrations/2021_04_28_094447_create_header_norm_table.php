<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderNormTable extends Migration
{
    public function up()
    {
        Schema::create('header_norm', function (Blueprint $table) {
            $table->id();
            $table->foreignId('header_id');
            $table->foreignId('norm_id');
            $table->timestamps();
            
            $table->foreign('norm_id')->references('id')->on('norms')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('header_id')->references('id')->on('headers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('header_norm');
    }
}
