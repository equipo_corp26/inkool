<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeadersTable extends Migration
{
    public function up()
    {
        Schema::create('headers', function (Blueprint $table) {
            $table->id();
            $table->string('name',80);
            $table->string('slug',90)->unique();
            $table->enum('type',['oval','normal']);
            $table->string('size');
            $table->string('color',80);
            $table->string('color_text',80)->default('#000000');
            $table->string('title',80);
            $table->string('sub_title',80)->nullable();
            $table->boolean('custom')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('headers');
    }
}
