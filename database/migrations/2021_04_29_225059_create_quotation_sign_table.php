<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationSignTable extends Migration
{
    public function up()
    {
        Schema::create('quotation_sign', function (Blueprint $table) {
            $table->id();
            $table->string('qty');
            $table->foreignId('quotation_id');
            $table->foreignId('sign_id');
            $table->timestamps();

            $table->foreign('sign_id')->references('id')->on('signs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('quotation_id')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('quotation_sign');
    }
}
