<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionsTable extends Migration
{
    public function up()
    {
        Schema::create('dimensions', function (Blueprint $table) {
            $table->id();
            $table->string('slug',90)->unique();
            $table->string('width');
            $table->string('height');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dimensions');
    }
}
