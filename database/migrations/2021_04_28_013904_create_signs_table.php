<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignsTable extends Migration
{
    public function up()
    {
        Schema::create('signs', function (Blueprint $table) {
            $table->id();
            $table->string('code',80);
            $table->string('slug',90)->unique();
            $table->string('norm')->nullable();
            $table->string('size',10)->default(0);
            $table->string('color',80);
            $table->string('color_line',80);
            $table->string('color_text',80);
            $table->string('pictogram')->nullable();
            $table->enum('header_type',['oval','normal'])->nullable();
            $table->string('header_color',80)->nullable()->default('#000000');
            $table->string('header_color_text',80)->nullable()->default('#000000');
            $table->string('header_size')->default(0);
            $table->string('title',80)->nullable();
            $table->string('sub_title',80)->nullable();
            $table->string('text1',80)->nullable();
            $table->string('text2',80)->nullable();
            $table->string('text3',80)->nullable();
            $table->string('text4',80)->nullable();
            $table->string('comment',80)->nullable();
            $table->string('material');
            $table->string('width');
            $table->string('height');
            $table->string ('type');
            $table->string('picture');
            $table->enum('status',['draft','published'])->default('draft');
            $table->foreignId('client_id');
            $table->foreignId('category_id');
            $table->timestamps();
            
            $table->foreign('client_id')->references('id')->on('clients')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('signs');
    }
}
