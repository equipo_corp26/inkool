<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionMaterialTable extends Migration
{
    public function up()
    {
        Schema::create('dimension_material', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dimension_id');
            $table->foreignId('material_id');
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('dimension_id')->references('id')->on('dimensions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('dimension_material');
    }
}
