<?php

namespace Database\Seeders;

use App\Models\Material;
use Illuminate\Database\Seeder;

class MaterialSeeder extends Seeder
{
    public function run()
    {
        Material::create(['name' => 'Laton Galvanizado 1,2mm']);
        Material::create(['name' => 'Plastico TROVISEL']);
        Material::create(['name' => 'Laton Galvanizado 1,5mm']);
        Material::create(['name' => 'Magnetico ( Lamina Imantada)']);
        Material::create(['name' => 'Madera Trupan']);
    }
}
