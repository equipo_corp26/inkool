<?php

namespace Database\Seeders;

use App\Models\Dimension;
use App\Models\Material;
use Illuminate\Database\Seeder;

class DimensionSeeder extends Seeder
{
    public function run()
    {
        Dimension::create([
            'height' => 10,
            'width' => 14,
        ])->materials()->sync(Material::where('slug','!=','aluminio-compuesto-intemperie')->pluck('id'));
        Dimension::create([
            'height' => 14,
            'width' => 20,
        ])->materials()->sync(Material::where('slug','!=','aluminio-compuesto-intemperie')->pluck('id'));
        Dimension::create([
            'height' => 20,
            'width' => 28,
        ])->materials()->sync(Material::all()->pluck('id'));
        Dimension::create([
            'height' => 30,
            'width' => 42,
        ])->materials()->sync(Material::all()->pluck('id'));
        Dimension::create([
            'height' => 40,
            'width' => 56,
        ])->materials()->sync(Material::all()->pluck('id'));
        Dimension::create([
            'height' => 50,
            'width' => 70,
        ])->materials()->sync(Material::all()->pluck('id'));
        Dimension::create([
            'height' => 60,
            'width' => 84,
        ])->materials()->sync(Material::all()->pluck('id'));
        Dimension::create([
            'height' => 'personalizar',
            'width' => 'personalizar',
        ])->materials()->sync(Material::all()->pluck('id'));
    }
}
