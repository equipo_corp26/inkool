<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Client::create([
            'email' => "generator@inkool.com"
        ]);
        /* MATERIALES */
        $this->call(MaterialSeeder::class);
        /* TYPE */
        $this->call(TypeSeeder::class);
        /* TIPOS */
        $this->call(ColorSeeder::class);
        /* CABECERAS */
        $this->call(HeaderSeeder::class);
        /* NORMAS */
        $this->call(NormSeeder::class);
        /* DIMENSIONES */
        $this->call(DimensionSeeder::class);
        /* CATEGORIAS */
        $this->call(CategorySeeder::class);
        /* PICTOGRAMAS */
        Storage::deleteDirectory('pictograms-folder');
        Storage::makeDirectory('pictograms-folder');
        Storage::deleteDirectory('demos');
        Storage::makeDirectory('demos');
        $this->call(PictogramSeeder::class);
        /* SIGN */
        $this->call(SignSeeder::class);
    }
}
