<?php

namespace Database\Seeders;

use App\Models\Header;
use Illuminate\Database\Seeder;

class HeaderSeeder extends Seeder
{
    public function run()
    {
        Header::create([
            'name'          => 'Peligro',
            'type'          => 'oval',
            'size'          => 'big',
            'color'         => '#000000',
            'color_text'    => '#ffffff',
            'title'         => 'PELIGRO',
            'custom'        => 0,
        ]);
        Header::create([
            'name'          => 'Precaucion',
            'type'          => 'normal',
            'size'          => 'medium',
            'color'         => '#000000',
            'color_text'    => '#f0f000',
            'title'         => 'PRECAUCION',
            'custom'        => 0,
        ]);
        Header::create([
            'name'          => 'Aviso',
            'type'          => 'normal',
            'size'          => 'medium',
            'color'         => '#0000ff',
            'color_text'    => '#ffffff',
            'title'         => 'AVISO',
            'custom'        => 0,
        ]);
        Header::create([
            'name'          => 'Advertencia',
            'type'          => 'normal',
            'size'          => 'medium',
            'color'         => '#ffa500',
            'title'         => 'ADVERTENCIA',
            'color_text'    => '#000000',
            'custom'        => 0,
        ]);
        Header::create([
            'name'          => 'Piense',
            'type'          => 'normal',
            'size'          => 'medium',
            'color'         => '#2ca637',
            'title'         => 'PIENSE',
            'color_text'    => '#ffffff',
            'custom'        => 0,
        ]);
    }
}
