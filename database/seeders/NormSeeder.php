<?php

namespace Database\Seeders;

use App\Models\Header;
use App\Models\Norm;
use Illuminate\Database\Seeder;

class NormSeeder extends Seeder
{
    public function run()
    {
        Norm::create(['name' => 'Norma Europea (ISO)' , 'background_type' => 'custom']);
        Norm::create(['name' => 'Norma Americana (ANSI)' , 'background_type' => 'simple'])->headers()->attach(Header::all()->pluck('id'));
        Norm::create(['name' => 'Norma Chilena 1411/2 y 2111 (ISO)' , 'background_type' => 'custom']);
        Norm::create(['name' => 'Norma Chilena 1411/1 (ANSI)' , 'background_type' => 'simple'])->headers()->attach(Header::all()->pluck('id'));
    }
}