<?php

namespace Database\Seeders;

use App\Models\Pictogram;
use Illuminate\Database\Seeder;

class PictogramSeeder extends Seeder
{
    public function run()
    {
        Pictogram::create(['url' => 'pictograms-folder/camion.png']);
        Pictogram::create(['url' => 'pictograms-folder/lavaojo.png']);
        Pictogram::create(['url' => 'pictograms-folder/no-fumar.png']);
        Pictogram::create(['url' => 'pictograms-folder/peligro-apilamiento.png']);
        Pictogram::create(['url' => 'pictograms-folder/peligro.png']);
        Pictogram::create(['url' => 'pictograms-folder/red-humeda.png']);
        Pictogram::create(['url' => 'pictograms-folder/red-inerte.png']);
    }
}
