<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    public function run()
    {
        Type::create(['name' => 'Adhesivo Reflectivo Impreso']);
        Type::create(['name' => 'Adhesivo Reflectivo Corte']);
        Type::create(['name' => 'Adhesivo PVC Normal Impreso']);
        Type::create(['name' => 'Adhesivo PVC en corte']);
        Type::create(['name' => 'Adhesivo Transparente']);
        Type::create(['name' => 'Adhesivo Fotoluminiscente']);
    }
}
