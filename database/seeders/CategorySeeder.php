<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {
        Category::create([
            'name'=>'seguridad',
        ]);
        Category::create([
            'name'=>'peligro',
            'parent'=> 1
        ]);
        Category::create([
            'name'=>'sustancia peligrosas',
            'parent'=> 1
        ]);
        Category::create([
            'name'=>'transito',
        ]);
        Category::create([
            'name'=>'transito vertical',
            'parent'=> 4
        ]);
        Category::create([
            'name'=>'transito horizontal',
            'parent'=> 4
        ]);
        Category::create([
            'name'=>'trabajos en la vida',
            'parent'=> 4
        ]);
        Category::create([
            'name'=>'buenas practicas',
        ]);
        Category::create([
            'name'=>'personalizado',
        ]);
    }
}
