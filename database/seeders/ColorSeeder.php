<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    public function run()
    {
        Color::create([
            'name'  => 'negro',
            'code' => '#000000',
            'line_code' => '#ffffff'
        ]);
        Color::create([
            'name'  => 'blanco',
            'code' => '#ffffff',
            'line_code' => '#000000'
        ]);
        Color::create([
            'name'  => 'azul',
            'code' => '#0000ff',
            'line_code' => '#ffffff'
        ]);
        Color::create([
            'name'  => 'amarillo',
            'code' => '#f0f000',
            'line_code' => '#000000'
        ]);
        Color::create([
            'name'  => 'verde',
            'code' => '#2ca637',
            'line_code' => '#ffffff'
        ]);
        Color::create([
            'name'  => 'rojo',
            'code' => '#e51420',
            'line_code' => '#ffffff'
        ]);
        Color::create([
            'name'  => 'anaranjado',
            'code' => '#ffa500',
            'line_code' => '#000000'
        ]);
    }
}
